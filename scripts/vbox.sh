#!/bin/bash

# https://github.com/geerlingguy/ansible-role-packer_rhel/blob/master/tasks/main.yml

if [[ -f "/home/vagrant/.vbox_version" ]]
then
    virtualbox_version=$(cat /home/vagrant/.vbox_version)

    mkdir -p /tmp/vbox
    mount -o ro -t iso9660 /home/vagrant/VBoxGuestAdditions_${virtualbox_version}.iso /tmp/vbox

    sh /tmp/vbox/VBoxLinuxAdditions.run

    umount /tmp/vbox
    rm -rf /tmp/vbox
    rm -f /home/vagrant/VBoxGuestAdditions_${virtualbox_version}.iso
    rm -f /home/vagrant/.vbox_version
fi
