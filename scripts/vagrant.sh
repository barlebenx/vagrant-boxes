#!/bin/bash
# Script for vagrant box requirements
# See at https://www.vagrantup.com/docs/boxes/base

## Sudo password-less
echo "vagrant        ALL=(ALL)       NOPASSWD: ALL" > /etc/sudoers.d/vagrant

## vagrant authorized_keys
mkdir -p /home/vagrant/.ssh
chmod 700 /home/vagrant/.ssh
curl --silent https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub > /home/vagrant/.ssh/authorized_keys
chmod 600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant: /home/vagrant/.ssh

## Set root password
echo -e "vagrant\nvagrant" | passwd --stdin root &> /dev/null

## SSH Tweaks
sed -i "s/^UseDNS.*/UseDNS no/g" /etc/ssh/sshd_config
sed -i "s/^GSSAPIAuthentication.*/GSSAPIAuthentication no/g" /etc/ssh/sshd_config
