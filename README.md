# Vagrant boxes

Project to build vagrant images with packer.

This project create the boxe and push it to the vagrant hub.

## Requirements

- [Packer from HashiCorp](https://www.packer.io/) version >= 1.6.1 (no test with previous).

## Build

### Vagrant cloud token

- Set token [here](https://app.vagrantup.com/settings/security)
- Add token on env var : `export VAGRANT_CLOUD_TOKEN=mysecrettoken`

### Launch build

Adapde the version number with vagrant cloud.

- Build of centos 8

```bash
packer build -var-file=vars/centos8.json -var='version=1.0.1' box-config.json
```

- Build of debian 10

```bash
packer build -var-file=vars/debian10.json -var='version=1.0.1' box-config.json
```
